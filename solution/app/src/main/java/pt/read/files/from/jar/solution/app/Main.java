package pt.read.files.from.jar.solution.app;

import java.io.IOException;

import pt.read.files.from.jar.solution.core.FileUtil;

public class Main {

	public static void main(String[] args) {
		
		try {
			
			FileUtil.readFile();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
