package pt.read.files.from.jar.solution.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class FileUtil {
	
	public static void readFile() throws IOException {

		StringBuilder resultStringBuilder = new StringBuilder();
		
		// Paths.get(Teste.class.getResource("/teste.txt").toURI());
		
		InputStream in = FileUtil.class.getResourceAsStream("/files/teste.txt");
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		
		String line;
        while ((line = reader.readLine()) != null) {
            resultStringBuilder.append(line).append("\n");
        }
        
        System.out.println(resultStringBuilder.toString());
		
	}
	
	public static void main(String[] args) {
		
		try {
			
			FileUtil.readFile();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
}
